from django.db import models
from django.contrib import admin
from django.utils import timezone

# Create your models here.
class Constate (models.Model):
	con_id = models.CharField(max_length = 600, verbose_name="容器短ID")
	time = models.DateTimeField(auto_now = True, verbose_name="更新時間")
	
	class Meta:
		verbose_name = 'Container'
		verbose_name_plural = 'Containers'
