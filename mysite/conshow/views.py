from django.shortcuts import render
from conshow.models import Constate
import docker
from django.http import HttpResponseRedirect
from django import forms
from conshow import info_composer

# Create your views here.
def docker_images(request):

	client = docker.from_env()
	all_images_object_list = []
	all_images_object_list = info_composer.dump_images_data(client)

	if request.method == 'POST':
		print (request.POST.getlist('con_creation'))
		'''
		a = request.POST.getlist('test')
		print (a[0])
		if a[0] == 'close container':
			b = request.POST.getlist('con_id')
			print (b[0])
			cur_con = client.containers.get(b[0])
			print (cur_con)
			cur_con.stop()
		if a[0] == 'open container':
			b = request.POST.getlist('con_id')
			print (b[0])
			cur_con = client.containers.get(b[0])
			print (cur_con)
			cur_con.start()
		'''
		request.session['_old_post'] = request.POST.getlist('con_creation')
		return HttpResponseRedirect('/docker_containers/')

		"""
		def next_view(request):
			old_post = request.session.get('_old_post')
			#do some stuff using old_post
		"""

	print (all_images_object_list)


	return render(request, 'docker_images.html',
					{
						'images' : all_images_object_list,
					})

def docker_containers(request):

	client = docker.from_env()

	if request.method == 'POST':
		print (request.POST.getlist('container_check'))
		request.session['_con_check'] = request.POST.getlist('container_check')
		return HttpResponseRedirect('/docker_container/')

	try:
		old_post = request.session.pop('_old_post')
		print ("old_post:")
		print (old_post)
		client.containers.run(old_post[0],"echo hello world",name = old_post[1])

	except:
		print("No new request for containers creation...")

	all_containers_object_list = []
	all_containers_object_list = info_composer.dump_containers_data(client)

	print (all_containers_object_list)


	return render(request, 'docker_containers.html',
					{
						'containers' : all_containers_object_list,
					})

def docker_container(request):
	client = docker.from_env()

	if request.method == 'POST':
		switch_list = None
		remove_cmd = None
		try:
			switch_list = request.POST.getlist('container_switch')
			print("switch_list")
			print(switch_list)
		except:
			print("No switch")
		try:
			remove_cmd = request.POST.getlist('container_remove')
			print("remove_cmd")
			print(remove_cmd)
		except:
			print("No remove")
		
		try:
			if switch_list != None:
				if switch_list[1] == '0':
					con = client.containers.get(switch_list[0])
					con.start()
				if switch_list[1] == '1':
					con = client.containers.get(switch_list[0])
					con.stop()
		except:
			print("No switch")
		try:
			if remove_cmd != None:
				con = client.containers.get(remove_cmd[0])
				con.remove()
		except:
			print("No remove")

	try:
		current_container_post = request.session.pop('_con_check')
		print ("current_container:")
		print (current_container_post)
		

	except:
		print("No new request for containers creation...")
		return HttpResponseRedirect('/docker_containers/')

	current_container = info_composer.dump_single_container(client, current_container_post[0])
	current_status = None
	if current_container_post[1] == 'ghost':
		current_status = 0
	if current_container_post[1] == 'up':
		current_status = 1	

	return render(request, 'docker_container.html',
					{
						'container' : current_container,
						'status' : current_status,
					})


