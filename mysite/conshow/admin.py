from django.contrib import admin
from conshow.models import Constate
# Register your models here.

admin.site.site_header = 'Docker to Web Demo'
admin.site.site_title = 'Docker to Web Demo'

class ConAdmin(admin.ModelAdmin):
	list_display = ('con_id', 'time', )
	ordering = ('time', )
	search_fields = ('con_id',)

admin.site.register(Constate, ConAdmin)