import docker
import humanize
from datetime import datetime, timedelta
import time

class image_data:

	def place_all_info(self, my_image):

		repo_split = my_image.attrs['RepoTags'][0].index(':')
		self.repo_name = my_image.attrs['RepoTags'][0][:repo_split]
		self.image_tag = my_image.attrs['RepoTags'][0][repo_split+1:]
		self.image_short_id = my_image.short_id[7:]  #get string after 'sha256:'
		self.image_long_id = my_image.id[7:]  #get string after 'sha256:'
		self.image_create_time_cpu = int(my_image.attrs['Created'])
		current_time = datetime.fromtimestamp(time.time())
		create_time_delta = current_time - datetime.fromtimestamp(self.image_create_time_cpu)
		self.image_create_time_humanize = humanize.naturaltime(create_time_delta)
		self.image_size_origin = int(my_image.attrs['Size'])
		self.image_size_humanize = humanize.naturalsize(int(self.image_size_origin))

	def __init__(self, my_image):
		self.repo_name = None
		self.image_tag = None
		self.image_short_id = None
		self.image_long_id = None
		self.image_create_time_cpu = None
		self.image_create_time_humanize = None
		self.image_size_origin = None
		self.image_size_humanize = None
		self.place_all_info(my_image)

def dump_images_data(my_client):
	images_iter = my_client.images
	response_list = []

	for my_image in images_iter.list(all = True):
		response_list.append(image_data(my_image))

	print (response_list)
	return response_list

class container_data:

	def find_image_name_by_id(self, con_id):

		client = docker.from_env()
		image_pool = client.images.list(all = True)
		for my_image in image_pool:
			repo_split = my_image.attrs['RepoTags'][0].index(':')
			repo_name = my_image.attrs['RepoTags'][0][:repo_split]
			repo_id = my_image.id
			if con_id == repo_id:
				return repo_name

		return "get image name fail..."

	def place_all_info(self, my_container):

		self.container_name = my_container.attrs['Name'][1:]
		self.container_short_id = my_container.short_id
		self.container_long_id = my_container.id
		self.image_name = self.find_image_name_by_id(my_container.attrs['Image'])
		self.image_long_id = my_container.attrs['Image'][7:]
		self.container_cmd = my_container.attrs['Config']['Cmd'][0] #list
		self.container_create_time_date = my_container.attrs['Created']
		current_time = datetime.fromtimestamp(time.time())
		gmt_fix_time = datetime.strptime(self.container_create_time_date[:-4], "%Y-%m-%dT%H:%M:%S.%f") + timedelta(hours = 8)
		create_time_delta = current_time - gmt_fix_time
		self.container_create_time_date_gmtfix = gmt_fix_time
		self.container_create_time_humanize = humanize.naturaltime(create_time_delta)
		self.container_status = my_container.status
		self.container_path = my_container.attrs['Path']
		self.container_args = my_container.attrs['Args']		

	def __init__(self, my_container):
		self.container_name = None
		self.image_long_id = None
		self.container_create_time_date_gmtfix = None
		self.container_short_id = None
		self.container_long_id = None
		self.image_name = None #need to check
		self.container_cmd = None
		self.container_create_time_date = None
		self.container_create_time_humanize = None
		self.container_status = None
		self.container_path = None
		self.container_args = None
		self.place_all_info(my_container)
		

def dump_containers_data(my_client):
	containers_iter = my_client.containers
	response_list = []

	for my_container in containers_iter.list(all = True):
		response_list.append(container_data(my_container))

	print (response_list)
	return response_list
	#return keyword pairs

def dump_single_container(my_client, id):
	my_container = my_client.containers.get(id)
	response_object = container_data(my_container)
	return response_object

def create_container_by_name(repo_name, container_custom_name):

	pass

def turn_up_container():

	pass

def turn_down_container():

	pass

def remove_container():

	pass